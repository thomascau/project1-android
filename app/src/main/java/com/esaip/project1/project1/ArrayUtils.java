package com.esaip.project1.project1;

/**
 * Created by Utilisateur on 23/10/2017.
 */

public class ArrayUtils {

    public static int[] removeIndex(int[] array, int index){
        int[] result = new int[array.length - 1];
        for (int i=0; i<array.length; i++){
            if(i < index)
                result[i] = array[i];
            else if (i > index)
                result[i] = array[i];
        }
        return result;
    }
}
