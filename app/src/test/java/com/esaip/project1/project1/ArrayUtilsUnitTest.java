package com.esaip.project1.project1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Utilisateur on 23/10/2017.
 */

public class ArrayUtilsUnitTest {
    @Test
    public void remove_in_array() throws Exception{
        int[] testArray = new int[]{1, 2, 3};
        int[] resultArray = ArrayUtils.removeIndex(testArray, 2);
        Assert.assertArrayEquals("Test int[] ArrayUtils.removeIndex(int[], int)",
                new int[]{1, 2}, resultArray);
    }
}
